package org.example.avro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvroExampleRunner {
    public static void main(String[] args) {
        SpringApplication.run(AvroExampleRunner.class, args);
    }
}
