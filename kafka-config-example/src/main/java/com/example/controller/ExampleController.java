package com.example.controller;

import com.example.service.KafkaProducer;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.OffsetDateTime;

import static com.example.config.KafkaTopics.exampleTopic;

@RestController
@RequiredArgsConstructor
public class ExampleController {

    private final KafkaProducer kafkaProducer;

    @GetMapping("/")
    public void getExample() {
        String message = "Message with time: " + OffsetDateTime.now();
        kafkaProducer.sendMessage(exampleTopic, message);
    }

}
