package com.example.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import static com.example.config.KafkaTopics.exampleTopic;

@Service
public class KafkaMessageListeners {

    @KafkaListener(topics = exampleTopic, groupId = "kafka-config-example")
    public void listenGroupFoo(String payload) {
        System.out.println(payload);
    }

}
